import { UEVueMixin } from "vue-uieditor";
import { data } from 'jquery';

// function _makeBaseComponent(type: string, hasChild?: boolean): UEVueMixin {
//   return {
//     functional: true,
//     render: function (h, context) {
//       if (!hasChild)
//         return h(type, context.data);
//       else {
//         // 完全透传任何 attribute、事件监听器、子节点等。
//         return h(type, context.data, context.children)
//       }
//     }
//   } as UEVueMixin
// }

const iviewUieditorTable = {
  functional: true,
  render: function (h, context) {
    const editing = context.props.editing === 'true';
    const data = context.data;
    if (editing) {
      const { staticClass } = data
      delete data.staticClass;
      const attrs = data.attrs || {};
      const { id, _meta_type } = attrs;
      delete attrs.id;
      delete attrs._meta_type;
      return h('div', {
        attrs: { id, _meta_type },
        staticClass
      }, [h('Table', data), ...(context.children || [])])
    } else {
      return h('Table', data, context.children)
    }
  },
  props: ['editing']
} as UEVueMixin;


// const iviewUieditorTag = {
//   functional: true,
//   render(h, context) {
//     return h('Tag', context.data, [context.props?.text]);
//   },
//   props: ['text']
// } as UEVueMixin;



export const IViewUEBaseComponents = {
  // 'iview-uieditor-tag': _makeBaseComponent('Tag', true)
  // 'iview-uieditor-tag': iviewUieditorTag
  'iview-uieditor-table': iviewUieditorTable
};